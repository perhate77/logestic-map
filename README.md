**This is a serial code for Logistic map (Bifurcation), written in C++.**

Compile using:


*   g++ LogisticMap.cpp



**Bifurcation**

At equilibrium or near equilibrium, there is only one steady state which depends
on some control parameter. If we follow the change in the state of the system
as the value of this parameter increases, the system is pushed farther and farther
away from equilibrium. At some point, a threshold of stability is reached that is
generally called a bifurcation point. At the bifurcation point, the system introduces
a characteristic time period(of the limit cycle) and the symmetry is broken. If the
parameter is increased further, one generates a succession of bifurcations that are
associated with the periodic doubling at each step. After such a period, doubling
cascade is over in the asymptotic limit, one finally gets the chaotic state. External
field like gravitational and magnetic field of the earth may play crucial role in the
mechanism at the point of bifurcation, leading to the self-organisation. In general,
dissipative structure depends critically on the condition in which it is formed. Thus
doubling of the period is one of the well known routes to chaos.


Ref.  _Computational Physics: An Introduction, R.C. Verma, etal. New Age International Publishers, New
Delhi(1999)_



![logesticMap](/uploads/1598817519b39d870b0d736de77459a1/logesticMap.png)![logesticMapWindow](/uploads/6f882657cd428db5b6590a33907d6225/logesticMapWindow.png)




Written By::<br/>
Rajneesh Kumar<br/>

Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,<br/>
Bengaluru 560064, India.<br/>
Email: rajneesh[at]jncasr.ac.in<br/>
27 Dec, 2020
